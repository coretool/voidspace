# Voidspace #
### Status: Working Beta ###
Voidspace let's programs share variables across processes, programs, and even devices!
Here is the [current todo list](https://bitbucket.org/Lite20/voidspace/wiki/Todo).

## In the box ##
When you fetch the VoidSpace module from NPM, you will not only get the module, but the VoidSpace server as well, which is the hub transferring data. Make sure an instance of the VoidSpace server is available at run-time of your application. Ideally, the module will eventually instantiate an instance with the global settings if it isn't already running.

## Usage ##
To use voidspace, you must first install the module with the following command-line command:
```
npm install --save voidspace
```

Now we can go ahead and use it! For every pool you want to work with, create a new Voidspace instance. Here's how we create an instance:
```
var testpool = require('voidspace');
```

### Connecting ###
Now we can connect to the voidspace server. If this is run on our local machine, we can do that with
```
testpool.connect();
```

Which will connect to localhost, port 3552. Port 3552 is the default port for the Voidspace server and is currently unconfigurable. If we don't have the server locally, we can pass a websocket URL to the server:
```
testpool.connect('ws://the-url.com:3552');
```

Don't forget to add port 3552!

### Joining and Creating ###
Once connected to the server we can now specify a pool. If we want to create a new pool we can use:
```
testpool.create("#pool-name");
```

And to join an existing pool we can use:
```
testpool.join("#pool-name");
```

Only use one of these per voidspace instance. If you attempt to create or join more than one pool with the same instance it will ignore the old pool and connect to the new and begin to glitch. Always make a new voidspace instance.

### Setting and Getting ###
We can easily set and get values. Continuing with our 'testpool' voidspace instance, to set a value we would use:
```
testpool.set('key', 'value');
```

where key is the key to access this value by later, and value is the actual data to store. Value can be almost anything. If it doesn't throw errors, it's working. We can also get set values using:
```
var value = testpool.get('key');
```

Using that will get the value from the pool with the key we pass, and assign the value to the value variable. There are currently no asynchronous methods for getting or setting values.

### Saving and Loading saved pools ###
Any pool can be saved regardless of whether your Voidspace instance is the owner, or not. You can save your pool to a file using:
```
testpool.save(__dirname + '/path-to-file.json');
```

Some important details are included here. **It's important that you add __dirname and a slash** as shown in the example. This is so node can build the file path correctly. In the future we will attempt to make this method more rigid and try to figure out where the file is when the first attempt to load fails. To load a file respectively you use:
```
testpool.load(__dirname + '/path-to-file.json');
```

It's really that easy!

# More #
If a feature is not outlined in the quick tutorial above, make sure to request an explanation on the repository via issues, and remind us to add an example!
