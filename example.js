var voidspace = require("voidspace"); // create a new voidspace client instance
voidspace.on('connect', function connected(data) { // create a connect handle
	console.log("Connected m8!");
	
});

voidspace.on('uuid', function uuid(data) { // create a 'uuid assinged' handle
	console.log("My uuid is: " + data.uuid);
});

voidspace.connect(); // connect to the server

// NOTE: This will not work if there isn't an instance of the voidspace server running!
