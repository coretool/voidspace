
var jwt = require('jwt-simple')

var exports = module.exports = {}

exports.createToken = function(pass, key, method, data, callback) {
    var payload = {
        pass: pass,
        data: data //data object
    }
    var token = jwt.encode(payload, key)
    callback( {'token' : token, 'method' : method})
}

exports.validateToken = function(token, key, pass, callback) {
    var method = token.method
    var token = jwt.decode(token.token, key)
    if (token.pass === pass) {
        return {
            success: true,
            data: token.data,
            method: method
        }
    } else {
        console.error("wrong password")
        callback( {
            success: false,
            data: null
        })
    }
}
