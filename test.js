var expect = require('expect.js');
var voidspace = require("./lib.js");
var fs = require("fs");
var data;
voidspace.on("uuid", function(d) {
	data = d;
});

describe('VoidSpace Client Tests:', function() {
	describe('Uuid:', function() {
		it('Returned data should be an object. ', function() {
			while (!data) {}
			expect(data).to.be.an('object');
		});

		it('Uuid should not be null, have spaces, and needs dashes. ', function() {
			while (!data) {}
			expect(data.uuid).to.not.be(undefined);
			expect(data.uuid).to.not.be('');
			expect(data.uuid).to.not.contain(' ');
			expect(data.uuid).to.contain('-');
		});

		it('Should be stored in the voidspace object as "voidspace.uuid". ', function() {
			while (!data) {}
			console.log("      Id is: " + voidspace.id());
			expect(data.uuid).to.be(voidspace.id());
		});
	});

	describe('Event handler:', function() {
		it('Handlers should be stored to be called. ', function() {
			voidspace.on("test1", function(data) {
				return true;
			});

			voidspace.on("test2", function(data) {
				return "This is a statement. ";
			});

			expect(voidspace.callback.test1()).to.be.ok();
			expect(voidspace.callback.test2()).to.eql("This is a statement. ");
		});

		it('Handlers should be be able to return preserved data. ', function() {
			voidspace.on("test2", function(data) {
				return "This is a statement. ";
			});

			expect(voidspace.callback.test2()).to.eql("This is a statement. ");
		});
	});

	describe('Data Transmission:', function() {
		describe('Inter-client:', function() {
			var clientone = require("./lib.js");
			var clienttwo = require("./lib.js");
			clientone.connect();
			clienttwo.connect();
			it('Creating pools. ', function() {
				clientone.create("#d@-t%$t-p**l!");
			});
			
			it('Joining pools. ', function() {
				clienttwo.join("#d@-t%$t-p**l!");
			});
			
			it('Setting and retrieving accross clients. ', function() {
				clientone.set("test-key", "eXaMPLe d@#tt&!!");
				expect(clienttwo.get("test-key")).to.be("eXaMPLe d@#tt&!!");
			});
			
			it('Resetting pools. ', function() {
				clientone.reset();
			});
		});
	});
	
	describe('Data Storage:', function() {
		describe('Operations: ', function() {
			it('Setting values. ', function () {
				voidspace.ws.id = "testID";
				voidspace.create("#test");
				voidspace.set("test-key", "eXaMPLe d@#tt&!!");
			});
			
			it('Getting values.', function() {
				expect(voidspace.get("test-key")).to.be("eXaMPLe d@#tt&!!");
			});
		});
		
		describe('Saving:', function() {
			it('Saving to file in location. ', function() {
				voidspace.datamap = {
					example: 'data'
				};
				voidspace.save(__dirname + '/testdata.json');
			});

			it('Should save valid json. ', function() {
				fs.readFile(__dirname + '/testdata.json', function(err, b) {
					JSON.parse(b.toString());
				});
			});
		});

		describe('Loading:', function() {
			it('Loading from file in location. ', function() {
				voidspace.load(__dirname + '/testdata.json');
				fs.unlink(__dirname + '/testdata.json');
			});

			it('Loading correct file data.', function() {
				expect(voidspace.datamap).to.eql({
					example: 'data'
				});
			});
		});
	});
});

voidspace.connect();
