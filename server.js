// A bad minifier messed up all the variable names!
require("colors");
var uuid = require("uuid");
var pools = {};
var WebSocketServer = require("ws").Server;
var wss = new WebSocketServer({
	port: 3552
});

console.log(tag() + "Server now listening on port 3552. ");
wss.on("connection", function(ws) {
	ws.id = uuid.v1();
	ws.send(JSON.stringify({
		req: "uuid",
		data: {
			uuid: ws.id
		}
	}));
	console.log(tag() + "Client connected and assigned id " + ws.id);
	ws.on("message", function(a) {
		var data = JSON.parse(a);
		if ("join" === data.req) {
			var d = pools[data.pool].clients;
			d[e.length] = ws;
			ws.send(JSON.stringify({
				req: "init",
				data: pools[data.pool].data
			}));
		} else if (data.req === "overwrite") {
			var pool = pools[data.pool];
			if (data.uuid === pool.owner) {
				pools[data.pool].data = data.data;
			}

			for (var f = 0; f < pool.clients.length; f++) {
				pool.clients[f].send(JSON.stringify({
					req: "init",
					data: pools[data.pool].data
				}));
			};
		} else if (data.req === "leave") {
			var e = pools[data.pool];
		} else if ("create" === data.req) {
			if (!pools[data.pool]) {
				pools[data.pool] = {
					data: {},
					clients: [],
					owner: data.uuid
				};
				ws.send(JSON.stringify({
					req: "create",
					data: {
						status: true
					}
				}));
				console.log(tag() + "Created pool " + data.pool);
			} else ws.send(JSON.stringify({
				req: "create",
				data: {
					status: false,
					reason: "That namespaces is already in use! "
				}
			}));
		} else if ("get" === data.req) {
			var e = pools[data.pool];
			ws.send(JSON.stringify({
				req: "init",
				data: e.data
			}));
		} else if ("set" === data.req) {
			var e = pools[data.pool];
			if(e.data) {
    			e.data[data.key] = data.data;
    			for (var f = 0; f < e.clients.length; f++) {
    				e.clients[f].send(a)
    			};
			}
		}
	});
});

function tag() {
	return "[".white + "Void".blue + "Space] ".white;
}
