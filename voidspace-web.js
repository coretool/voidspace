// This is the voidspace client for web browsers
// A bad minifier messed up all the variable names!
var Voidspace = function() {
    this.datamap = {};
    this.callback = {};
    this.rpc = {};
    this.debug = false;
    this.tag = "";
};

Voidspace.prototype.on = function(a, b) {
    this.callback[a] = b;
};

Voidspace.prototype.id = function() {
    return this.ws.id;
};

Voidspace.prototype.rpcCall = function(call, args, callback) {
    this.ws.send(JSON.stringify({
		      req: 'rpc',
		      data: {
		          call: call,
		          arga: args
		      }
	   }));
};

Voidspace.prototype.reset = function(callback) {
	this.ws.send(JSON.stringify({
		req: 'overwrite',
		data: {}
	}));
    if(callback) {
    	callback();
    }
};

Voidspace.prototype.connect = function(a) {
    var b = this.callback;
    if (!a) a = "ws://127.0.0.1:3552";
    this.ws = new WebSocket(a);
    this.ws.onopen = function() {
        if (this.debug) console.log("[VS][INFO] Websocket connected. ");
        if (void 0 !== b["connect"]) b["connect"]({
            e: "connect",
            dest: a
        });
    };
    this.ws.onmessage = function(a, c) {
        if (this.debug) console.log("[VS][INBOUND] " + a);
        var d = JSON.parse(a);
        if ("set" === d.req) {
            this.datamap[d.tag] = d.data;
       	} else if ("create" === d.req) {
            b["create"](d.data);
       	} else if ("init" === d.req) {
            this.datamap = d.data;
       	} else if ("uuid" === d.req) {
            this.id = d.data.uuid;
            b["uuid"](d.data);
        } else if (d.req === "rpc") {
            this.rpc[d.data.call](d.data.args);
        }
    };
};

Voidspace.prototype.disconnect = function() {
    this.ws.close();
};

Voidspace.prototype.get = function(a) {
    return this.datamap[a];
};

Voidspace.prototype.set = function(k, b) {
	if(k) {
	    if (this.debug) {
	        console.log("[VS][INFO] Setting '" + k + "' to '" + b + "'. ");
	        console.log("[VS][OUTBOUND] " + JSON.stringify({
	            req: "set",
	            key: k,
	            pool: this.pool,
	            data: b
	        }));
	    }
	    this.datamap[k] = b;
	    this.ws.send(JSON.stringify({
	        req: "set",
	        pool: this.pool,
	        data: b,
	        key: k
	    }));
	} else {
		console.log("[VS][WARN] No key provided! Not setting value. ");
	}
};

Voidspace.prototype.create = function(a, b) {
    if (this.debug) {
        console.log("[VS][INFO] Creating namespace '" + a + "'. ");
        console.log("[VS][OUTBOUND] " + JSON.stringify({
            req: "create",
            pool: a,
            uuid: this.ws.id
        }));
    }
    this.pool = a;
    this.ws.send(JSON.stringify({
        req: "create",
        pool: a
    }));
    this.callback["create"] = b;
};

Voidspace.prototype.join = function(a) {
    if (this.debug) {
        console.log("[VS][INFO] Joining namespace '" + a + "'. ");
        console.log("[VS][OUTBOUND] " + JSON.stringify({
            req: "join",
            pool: a
        }));
    }
    this.pool = a;
    this.ws.send(JSON.stringify({
        req: "join",
        pool: a
    }));
};

Voidspace.prototype.tag = function(tag) {
    this.ws.send(JSON.stringify({
        req: "tag",
        data: {
            tag: tag
        }
    }));
};

Voidspace.prototype.save = function(a) {
    //
};

Voidspace.prototype.load = function(a) {
    //
};